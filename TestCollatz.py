#!/usr/bin/env python3

# --------------
# TestCollatz.py
# --------------

# -------
# imports
# -------

from io       import StringIO
from unittest import main, TestCase

from Collatz import collatz_read, collatz_eval, collatz_print, collatz_solve

# -----------
# TestCollatz
# -----------

class TestCollatz (TestCase) :
    # ----
    # read
    # ----

    def test_read (self) :
        s    = "1 10\n"
        i, j = collatz_read(s)
        self.assertEqual(i,  1)
        self.assertEqual(j, 10)

    # ----
    # eval
    # ----

    def test_eval_1 (self) :
        v = collatz_eval(1, 10)
        self.assertEqual(v, 20)

    def test_eval_2 (self) :
        v = collatz_eval(100, 200)
        self.assertEqual(v, 125)

    def test_eval_3 (self) :
        v = collatz_eval(201, 210)
        self.assertEqual(v, 89)

    def test_eval_4 (self) :
        v = collatz_eval(900, 1000)
        self.assertEqual(v, 174)

    def test_eval_5 (self) :
        v = collatz_eval(524124, 213543)
        self.assertEqual(v,  470) 

    def test_eval_6 (self) :
        v = collatz_eval(6845, 635465)
        self.assertEqual(v, 509) 

    def test_eval_7 (self) :
        v = collatz_eval(4654, 23144)
        self.assertEqual(v, 279) 

    def test_eval_8 (self) :
        v = collatz_eval(20, 10000)
        self.assertEqual(v, 262)  

    def test_eval_9 (self) :
        v = collatz_eval(63545, 100)
        self.assertEqual(v, 340) 

    def test_eval_10 (self) :
        v = collatz_eval(10000, 10)
        self.assertEqual(v, 262) 

    def test_eval_11 (self) :
        v = collatz_eval(8989, 8990)
        self.assertEqual(v,  79) 

    def test_eval_12 (self) :
        v = collatz_eval(1500, 1600)
        self.assertEqual(v,  167) 

    def test_eval_13 (self) :
        v = collatz_eval(99999, 999)
        self.assertEqual(v, 351) 

    def test_eval_14 (self) :
        v = collatz_eval(651651, 32154)
        self.assertEqual(v, 509) 

    def test_eval_15 (self) :
        v = collatz_eval(321564, 56321)
        self.assertEqual(v, 443) 

    def test_eval_16 (self) :
        v = collatz_eval(2582, 225252)
        self.assertEqual(v, 386) 

    def test_eval_17 (self) :
        v = collatz_eval(222222, 222223)
        self.assertEqual(v, 117) 

    def test_eval_18 (self) :
        v = collatz_eval(44445, 55554)
        self.assertEqual(v, 340) 

    def test_eval_19 (self) :
        v = collatz_eval(66666, 668789)
        self.assertEqual(v, 509)

    # -----
    # print
    # -----

    def test_print (self) :
        w = StringIO()
        collatz_print(w, 1, 10, 20)
        self.assertEqual(w.getvalue(), "1 10 20\n")

    # -----
    # solve
    # -----

    def test_solve (self) :
        r = StringIO("1 10\n100 200\n201 210\n900 1000\n")
        w = StringIO()
        collatz_solve(r, w)
        self.assertEqual(w.getvalue(), "1 10 20\n100 200 125\n201 210 89\n900 1000 174\n")

# ----
# main
# ----

if __name__ == "__main__" : #pragma: no cover
    main()
